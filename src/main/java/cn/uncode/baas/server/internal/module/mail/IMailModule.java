package cn.uncode.baas.server.internal.module.mail;

import cn.uncode.baas.server.exception.ValidateException;

public interface IMailModule {
	
	boolean sendMail(Object param)throws ValidateException;

}
