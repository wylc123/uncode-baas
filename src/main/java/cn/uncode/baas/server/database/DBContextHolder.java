package cn.uncode.baas.server.database;

public class DBContextHolder {
	
	private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<String>();

	public static void setDbType(String dbType) {
		CONTEXT_HOLDER.set(dbType);
	}

	public static String getDbType() {
		return CONTEXT_HOLDER.get();
	}

	public static void clearDbType() {
		CONTEXT_HOLDER.remove();
	}
	
}
